\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Background}{1}
\contentsline {subsection}{\numberline {2.1}Piezoelectric Material}{1}
\contentsline {subsection}{\numberline {2.2}Properties to be varied}{1}
\contentsline {section}{\numberline {3}Equations}{2}
\contentsline {subsection}{\numberline {3.1}Structural Equation}{2}
\contentsline {subsection}{\numberline {3.2}Strain to voltage equation}{2}
\contentsline {section}{\numberline {4}Geometric Dimensions}{2}
