
function output=system_of_ODEs(varargin)
%this function returns the rate of mass concentration of subject A and B
%variables are t,c,k1,k2,k3,k4
%%
% $t=time\quad in \quad hour;c=c_A,c_B= mass \quad concentration\quad in\quad mg/L;k_1,k_2,k_3= rate\quad constatnt\quad in\quad 1/h$
%%
% $k_4=\quad rate\quad constant\quad in\quad mg/l/h$
%two concentrations cA and cB
%outputs are rate of mass concentration of A and B 

% the differential equations of rate of mass concentration of A and B
%%
% $\frac{dc_A}{dt}=-k_1*c_A-k_2*c_A$ 
%%
% $\frac{dc_B}{dt}=k_1*c_A-k_3-k_4*c_B$


if nargin==0 %argument input
    t=0;
    c=[6.25 0];
else 
    t=varargin{1};
    c=varargin{2};
end

if nargin==6
        k1=varargin{3};
        k2=varargin{4};
        k3=varargin{5};
        k4=varargin{6};
else
    k1=.15; %default values
    k2=.6;
    k3=.1;
    k4=.2;
end
   cA=c(1);
   cB=c(2);
dcAdt=-k1*cA-k2*cA;
dcBdt=k1*cA-k3-k4*cB;
["t","cA","cB","k1","k2","k3","k4";t c k1 k2 k3 k4]
output=[dcAdt;dcBdt]; %output
end 


