# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 20:56:10 2017

@author: hossa
"""
import numpy as np

from scipy.optimize import curve_fit

from scipy.integrate import odeint

import matplotlib.pyplot as plt


timedata = np.array( [1/60, 1/30, 1/20, 1/10] ) # time

ydata = np.array( [[0.5074, 0.3796, 0.2882, 0.1762], [0.3767, 0.4385, 0.4865, 0.5416], [0.1159, 0.1819, 0.2253, 0.2822 ]] )
conversion=np.array([0.4926, 0.6204, 0.7118, 0.8237])
params_guess = np.array([1,0.5,0.5])
y0=np.array([1,0,0])
def param_estim_3lump(y,t,args):

    dydt=[]
    k1 = args[0]
    k2 = args[1]
    k3 = args[2]
    dydt.append(-(k1+k3)*y[0]**2)
    dydt.append(k1*y[0]**2-k2*y[1])
    dydt.append(k3*y[0]**2+k2*y[1])
    dydt = np.transpose(dydt)
    dydt=  np.array(dydt)
    print(k1,k2,k3)
    return dydt.ravel()

def ODEmodel1(timedata,*params):

    y0=np.array([1,0,0])
    y_output=[]

    for i in np.arange(len(timedata)):
        t_inc = 0.01
        tspan = np.arange(0,timedata[i]+t_inc,t_inc)
        y_calc = odeint(param_estim_3lump,y0,tspan,args=(params,))
        y_output.append(y_calc[-1,:])
    y_output=np.transpose(y_output)
    y_output=np.array(y_output)      
    return y_output.ravel()

params_output, pcov = curve_fit(ODEmodel1,timedata,ydata.ravel(),p0=params_guess)
y0=np.array([1,0,0])
y_calculatedguess = odeint(param_estim_3lump,y0,timedata,args=(params_guess,))
y_calculated = odeint(param_estim_3lump,y0,timedata,args=(params_output,))
#print(y_calculated[:,0])
plt.figure(1)
plt.plot(timedata,ydata[0,:],label='y1 fit')
plt.plot(timedata,ydata[1,:],label='y2 fit')
plt.plot(timedata,ydata[2,:],label='y3 fit')
plt.legend(loc='upper right')
plt.xlabel('time')
plt.ylabel('ydata')
plt.show()
plt.figure(2)
plt.plot(conversion, y_calculated[:,0], label='VGO')
plt.plot(conversion, y_calculated[:,1], label='Gasoline')
plt.plot(conversion, y_calculated[:,2], label='Gas and Coke')
plt.legend(loc='upper right')
plt.xlabel('conversion,wt frac')
plt.ylabel('yield, wt frac')
plt.show()