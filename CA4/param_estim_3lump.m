function param_estim_3lump

tdata = [1/60 1/30 1/20 1/10]; %time
ydata = [.5074 .3796 .2882 .1762; .3767 .4385 .4865 .5416; .1159 .1819 .2253 .2822];
conversion=[.4926 .6204 .7118 .8238];
%%
% $\frac{dy_1}{dt}=-(k1+k3)y_{1}^{2}$
%%
% $\frac{dy_2}{dt}=k1y_{1}^{2}-k2y_2$
%%
% $\frac{dy_3}{dt}=k3y_{1}^{2}+k2y_2$
%initial guess
k(1)=1;
k(2)=0.5;
k(3)=0.5;
params_guess = k;

%initial conditions
y0(1)=1;
y0(2)=0;
y0(3)=0;

[params,~] = lsqcurvefit(@(params,tdata) ...
    ODEmodel1(params,tdata,y0),params_guess,tdata,ydata, [], []);


k1=params(1);
k2=params(2);
k3=params(3);
figure(1)
plot(tdata,ydata(1,:),'r');
hold on
plot(tdata,ydata(2,:),'b');
hold on
plot(tdata,ydata(3,:),'y');
xlabel('time');
ylabel('y');
legend('y1 data','y2 data','y3 data','Location','best');
hold off

figure(2)
y_calc = ODEmodel1(params,tdata,y0);
plot(conversion,y_calc(1,:),'r-o');
hold on
plot(conversion,y_calc(2,:),'b-d');
hold on 
plot(conversion,y_calc(3,:),'g-d');
xlabel('conversion, wt frac');
ylabel('yield, wt frac');
legend('VGO','Gasoline','Gas+Coke','Location','best');
hold off 

function dydt = ODE1_definition(t,y,params)

    k1 = params(1);
    k2 = params(2);
    k3 =params(3);
    dydt(1)=-(k1+k3)*y(1)^2;
    dydt(2)=k1*y(1)^2-k2*y(2);
    dydt(3)=k3*y(1)^2+k2*y(2);
    dydt=dydt';

end

function y_output = ODEmodel1(params,tdata,y0)
    for i = 1:length(tdata);
        tspan = [0:0.01:tdata(i)];
        [~,y_calc] = ode23s(@(t,y) ODE1_definition(t,y,params),tspan,y0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output=y_output';
end

end 