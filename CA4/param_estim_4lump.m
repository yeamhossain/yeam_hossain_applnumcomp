function param_estim_4lump

tdata = [1/60 1/30 1/20 1/10]; %time
ydata = [.5074 .3796 .2882 .1762; .3767 .4385 .4865 .5416; .0885 .136 .1681 .2108; .0274 .0459 .0572 .0714];
conversion=[.4926 .6204 .7118 .8238];
%%
% $\frac{dy_1}{dt}=-(k_{12}+k_{13}+k_{14}y_{1}^{2}$
%%
% $\frac{dy_2}{dt}=k_{12}y_{1}^{2}-k_{23}y_{2}-k_{24}y_{2}$
%%
% $\frac{dy_3}{dt}=k_{13}y_{1}^{2}-k_{23}y_{2}$
%%
% $\frac{dy_4}{dt}=k_{14}y_{1}^{2}-k_{24}y_{2}$
k(1)=1;
k(2)=.5;
k(3)=.3;
k(4)=0;
k(5)=.1;
params_guess = k;

y0(1) = 0.7;
y0(2) = .2;
y0(3)=.04;
y0(4)=.01;


[params,resnorm,residuals,eyitflag,] = lsqcurvefit(@(params,tdata) ...
    ODEmodel2(params,tdata,y0),params_guess,tdata,ydata, [], []);
% 
figure(1)
plot(tdata,ydata(1,:),'r');
hold on
plot(tdata,ydata(2,:),'b');
plot(tdata,ydata(3,:),'g');
hold on
plot(tdata,ydata(4,:),'y');
xlabel('time');
ylabel('y');
legend('y1 ','y2 ','y3','y4','Location','best');
hold off
figure(2)
y_calc = ODEmodel2(params,tdata,y0);
plot(conversion,y_calc(1,:),'r-o');
hold on 
plot(conversion,y_calc(2,:),'b-d');
hold on
plot(conversion,y_calc(3,:),'g-d');
hold on
plot(conversion,y_calc(4,:),'y-d');
xlabel('conversion, wt frac');
ylabel('yield, wt frac');
legend('VGO','Gasoline','Gas','Coke','Location','best');
hold off 

end 
function dydt = ODE2(t,y,params)

    k12= params(1);
    k13 = params(2);
    k14=params(3);
    k23=params(4);
    k24=params(5);
    dydt(1) = -(k12+k13+k14)*y(1)^2;
    dydt(2) = k12*y(1)^2-k23*y(2)-k24*y(2);
    dydt(3)=k13*y(1)^2+k23*y(2);
    dydt(4)=k14*y(1)^2+k24*y(2);
    dydt = dydt';
end
function y_output = ODEmodel2(params,tdata,y0)

    for i = 1:length(tdata);

        tspan = [0:0.01:tdata(i)];

        [~,y_calc] = ode23s(@(t,y) ODE2(t,y,params),tspan,y0);

        y_output(i,:)=y_calc(end,:);

    end

    y_output = y_output';

end
