function varargout = sub_GUI(varargin)
% SUB_GUI MATLAB code for sub_GUI.fig
%      SUB_GUI, by itself, creates a new SUB_GUI or raises the existing
%      singleton*.
%
%      H = SUB_GUI returns the handle to a new SUB_GUI or the handle to
%      the existing singleton*.
%
%      SUB_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SUB_GUI.M with the given input arguments.
%
%      SUB_GUI('Property','Value',...) creates a new SUB_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sub_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sub_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sub_GUI

% Last Modified by GUIDE v2.5 11-Dec-2017 20:15:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sub_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @sub_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sub_GUI is made visible.
function sub_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sub_GUI (see VARARGIN)

% Choose default command line output for sub_GUI
handles.output = hObject;
addpath(genpath('export_fig'))
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sub_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sub_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
% input_diffeqtns=get(handles.edit3,'String')
% input_diffeqtns=str2double(input_diffeqtns);
% save('input_diffeqtns')

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=eqtns;
Number_equations=get(handles.edit1,'String') 
Number_equations=str2num(Number_equations);
save('Number_equations.mat','Number_equations');
for i=1:Number_equations
    run('eqtns')
    uiwait
    load('diffeqtns.mat')
    odes{i}=strcat('d',numerator,'/d',denom,'=',diffeqtn)
handles.diff_eqtn{i}=diffeqtn
handles.numerator{i}=numerator
handles.denominator{i}=denom
handles.Initial_value(i)=str2num(ICvalue)
guidata(hObject,handles)
end
set(handles.ODEs,'String',odes)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure1.UserData=explicit_eqtns;
Number_explicit=get(handles.edit2,'String')
Number_explicit=str2num(Number_explicit);
save('Number_explicit.mat','Number_explicit');

if Number_explicit==0,
    handles.Nexp={}
else
for i=1:Number_explicit
 run('explicit_eqtns')
 uiwait
 load('explicit_eqtns.mat','expliciteqtn')
 explicit{i}=expliciteqtn;
handles.Nexp{i}=expliciteqtn
guidata(hObject,handles)
end
end

set(handles.Explicit_equations,'String',explicit)



% --- Executes on button press in Solve.
function Solve_Callback(hObject, eventdata, handles)
% hObject    handle to Solve (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tinit=get(handles.Tinit,'String');
tinit=str2num(tinit)
tfinal=get(handles.Tfinal,'String');
tfinal=str2num(tfinal)

[t,y]=ode45(@(t,y)ODE(t,y,handles.diff_eqtn,handles.numerator,handles.denominator,handles.Nexp),[tinit tfinal],handles.Initial_value);
t;
y;

handles.t=t;
handles.y=y;
guidata(hObject,handles)


axes(handles.plots);

load('Number_equations.mat','Number_equations')
% neqn=Number_equations
for i=1:Number_equations
p(i)=plot(t,y(:,i))
hold all
legend_labels{i} = sprintf('%s', handles.numerator{i})

end 

legend(p,legend_labels{:})

xlabel(handles.denominator(1))
ylabel('y')


function dydt = ODE(t, y,diff_eqtn,numerator,denominator,Nexp)


load('explicit_eqtns.mat','expliciteqtn')

 % Input: t and y are the independent and dependent variable values
 % denominators, numerators, RHSs, and explicitEqns are cell
% arrays with the first three terms defining the ODEs of the form
 % d(numerators) / d(denominators) = RHSs and the fourth term
 % defining the associated explicit equations
 % Output: the derivative vector dy/dt for y(1):y(numODEs) where
 % numODEs = length(numerators) = length(RHSs) = length(denominators)

% independent variable
 str0=cell2mat(denominator(1));
 eval(strcat(str0,'= t;'));

 % dependent variables
    for i = 1:length(numerator)
    str1 = cell2mat(numerator(i));
    eval(strcat(str1,'= y(i);'));
    end

% explicit equations provided in MATLAB acceptable order, ...
% semicolon separated list

if length(Nexp)>0
 for i = 1:length(Nexp)
 str2 = cell2mat(Nexp(i));
 eval([str2,';']);
 end
end  
 % Right-hand sides of ODE definitions
 for i = 1:length(diff_eqtn)
    str3 = cell2mat(diff_eqtn(i));
    dydt(i) = eval(str3);
 end

 dydt = dydt';

function Tinit_Callback(hObject, eventdata, handles)
% hObject    handle to Tinit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tinit as text
%        str2double(get(hObject,'String')) returns contents of Tinit as a double
% global tinit
% tinit=get(hObject,'String');

% --- Executes during object creation, after setting all properties.
function Tinit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tinit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tfinal_Callback(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tfinal as text
%        str2double(get(hObject,'String')) returns contents of Tfinal as a double
% global tf
% tf=get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function Tfinal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function plots_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate plots


% --- Executes on button press in pushbutton4.


% --- Executes on button press in Save_plot.
function Save_plot_Callback(hObject, eventdata, handles)
% hObject    handle to Save_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ax=handles.plots
figure_handle=isolate_axes(ax)
export_fig graph_plots.png
% --- Executes during object creation, after setting all properties.
function ODEs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ODEs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function Explicit_equations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Explicit_equations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Exportxls.
function Exportxls_Callback(hObject, eventdata, handles)
% hObject    handle to Exportxls (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB

xlswrite('data_result.xls',handles.denominator,'Sheet 1','A1')
xlswrite('data_result.xls',handles.numerator,'Sheet 1','B1')

xlswrite('data_result.xls',handles.t,'Sheet 1','A2')
xlswrite('data_result.xls',handles.y,'Sheet 1','B2')
% handles    structure with handles and user data (see GUIDATA)
