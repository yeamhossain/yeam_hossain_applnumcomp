function varargout = eqtns(varargin)
% EQTNS MATLAB code for eqtns.fig
%      EQTNS, by itself, creates a new EQTNS or raises the existing
%      singleton*.
%
%      H = EQTNS returns the handle to a new EQTNS or the handle to
%      the existing singleton*.
%
%      EQTNS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EQTNS.M with the given input arguments.
%
%      EQTNS('Property','Value',...) creates a new EQTNS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before eqtns_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to eqtns_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help eqtns

% Last Modified by GUIDE v2.5 08-Dec-2017 11:25:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @eqtns_OpeningFcn, ...
                   'gui_OutputFcn',  @eqtns_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before eqtns is made visible.
function eqtns_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to eqtns (see VARARGIN)

% Choose default command line output for eqtns
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes eqtns wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = eqtns_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% p=load('x1.txt')


diffeqtn=get(handles.edit3,'String')
numerator=get(handles.numerator,'String')
denom=get(handles.denominator,'String')
ICvalue=get(handles.initialval,'String')
save('diffeqtns.mat','diffeqtn','numerator','denom','ICvalue')

% diffeqtn=str2double(diffeqtn)
close('eqtns')


% dyexpression=get(handles.edit3,'String');
% dyexpression=str2num(dyexpression);
% save('dyexpression.txt','dyexpression','-ascii')
% numerator=get(handles.numerator,'String');
% denominator=get(handles.denominator,'String');
% init=get(handles.initialval,'String');


function numerator_Callback(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numerator as text
%        str2double(get(hObject,'String')) returns contents of numerator as a double

save


% --- Executes during object creation, after setting all properties.
function numerator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numerator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function denominator_Callback(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of denominator as text
%        str2double(get(hObject,'String')) returns contents of denominator as a double


% --- Executes during object creation, after setting all properties.
function denominator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to denominator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% dyexpression=get(handles.edit3,'String');
% dyexpression=str2num(dyexpression);
% save('dyexpression.txt','dyexpression','-ascii')
% numerator=get(handles.numerator,'String');
% denominator=get(handles.denominator,'String');
% init=get(handles.initialval,'String');
% init=str2num(init);
% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double




% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function initialval_Callback(hObject, eventdata, handles)
% hObject    handle to initialval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initialval as text
%        str2double(get(hObject,'String')) returns contents of initialval as a double


% --- Executes during object creation, after setting all properties.
function initialval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initialval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tinitial_Callback(hObject, eventdata, handles)
% hObject    handle to Tinitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tinitial as text
%        str2double(get(hObject,'String')) returns contents of Tinitial as a double


% --- Executes during object creation, after setting all properties.
function Tinitial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tinitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tfinal_Callback(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tfinal as text
%        str2double(get(hObject,'String')) returns contents of Tfinal as a double


% --- Executes during object creation, after setting all properties.
function Tfinal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tfinal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
