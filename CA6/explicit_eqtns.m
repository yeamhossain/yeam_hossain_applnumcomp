function varargout = explicit_eqtns(varargin)
% EXPLICIT_EQTNS MATLAB code for explicit_eqtns.fig
%      EXPLICIT_EQTNS, by itself, creates a new EXPLICIT_EQTNS or raises the existing
%      singleton*.
%
%      H = EXPLICIT_EQTNS returns the handle to a new EXPLICIT_EQTNS or the handle to
%      the existing singleton*.
%
%      EXPLICIT_EQTNS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXPLICIT_EQTNS.M with the given input arguments.
%
%      EXPLICIT_EQTNS('Property','Value',...) creates a new EXPLICIT_EQTNS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before explicit_eqtns_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to explicit_eqtns_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help explicit_eqtns

% Last Modified by GUIDE v2.5 08-Dec-2017 13:11:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @explicit_eqtns_OpeningFcn, ...
                   'gui_OutputFcn',  @explicit_eqtns_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before explicit_eqtns is made visible.
function explicit_eqtns_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to explicit_eqtns (see VARARGIN)

% Choose default command line output for explicit_eqtns
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes explicit_eqtns wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = explicit_eqtns_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function expliciteqtn_Callback(hObject, eventdata, handles)
% hObject    handle to explicit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of explicit as text
%        str2double(get(hObject,'String')) returns contents of explicit as a double



% --- Executes during object creation, after setting all properties.
function expliciteqtnfield_CreateFcn(hObject, eventdata, handles)
% hObject    handle to explicit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LHS_Callback(hObject, eventdata, handles)
% hObject    handle to LHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LHS as text
%        str2double(get(hObject,'String')) returns contents of LHS as a double


% --- Executes during object creation, after setting all properties.
function LHS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LHS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
expliciteqtn=get(handles.explicit,'String')
save('explicit_eqtns.mat','expliciteqtn')
close('explicit_eqtns')



function explicit_Callback(hObject, eventdata, handles)
% hObject    handle to explicit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of explicit as text
%        str2double(get(hObject,'String')) returns contents of explicit as a double


% --- Executes during object creation, after setting all properties.
function explicit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to explicit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on explicit and none of its controls.
function explicit_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to explicit (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
